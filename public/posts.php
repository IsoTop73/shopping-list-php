<?php

include('../app/bootstrap.php');

if (empty($_SESSION['id'])) {
    header('Location: /');
}

$user_id = $_GET['user'];
$items = ShoppingList::getItems($user_id);

include('../views/home.php');
