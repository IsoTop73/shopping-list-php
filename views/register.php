<!DOCTYPE html>
<html lang="en">
<head>
    <?php include '../views/includes/head.php' ?>
</head>
<body>

    <?php include '../views/includes/navbar.php'; ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        <form enctype="multipart/form-data" action="register.php" method="POST">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control" placeholder="First Name" name="first_name">
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control" placeholder="Last Name" name="last_name">
                            </div>
                            <div class="form-group">
                                <label>Email address</label>
                                <input type="email" class="form-control" placeholder="Email" name="email">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" placeholder="Password" name="password">
                            </div>
                            <div class="form-group">
                                <label>Confirm password</label>
                                <input type="password" class="form-control" placeholder="Confirm password" name="confirm_password">
                            </div>
                            <div class="form-group">
                                <label>Avatar</label>
                                <input type="file" name="avatar">
                            </div>
                            <button type="submit" class="btn btn-default btn-block">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>