<!DOCTYPE html>
<html lang="en">
<?php include '../views/includes/head.php' ?>
<body id="home">

<?php include '../views/includes/navbar.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">

            <?php if (isset($_SESSION['id'])): ?>

                <div class="panel panel-primary">
                    <div class="panel-heading">To buy</div>
                    <div class="panel-body">

                        <?php if (count($items)): ?>

                            <ul class="list-group">
                                <?php foreach ($items as $row) { ?>
                                    <li class='list-group-item clearfix'>
                                        <strong>
                                            <?php echo $row['title'] ?>
                                        </strong>
                                        <small>Created: <?php echo $row['c_time']; ?></small>
                                        <div class="controls pull-right">
                                            <a href="<?php echo 'editItem.php?id=' . $row['id'] . (isset($user_id) ? ('&user=' . $user_id) : ''); ?>"
                                               class="btn btn-primary btn-xs">
                                                Edit
                                            </a>
                                            <a href="<?php echo 'deleteItem.php?id=' . $row['id'] . (isset($user_id) ? ('&user=' . $user_id) : ''); ?>"
                                               class="btn btn-danger btn-xs">
                                                Delete
                                            </a>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>

                            <hr>

                        <?php endif; ?>

                        <form class="item-form" class="form" method="POST" action="addItem.php">
                            <label>New item</label>
                            <div class="form-group">
                                <input name="title" type="text" class="form-control" placeholder="Title" autofocus>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="description" placeholder="Description"
                                          rows="3"></textarea>
                            </div>
                            <?php if (isset($user_id)): ?>
                                <input type="hidden" name="user" value="<?php echo $user_id; ?>">
                            <?php endif; ?>
                            <button type="submit" class="btn btn-success">Add item</button>
                        </form>
                    </div>
                </div>

            <?php endif; ?>

        </div>
    </div>
</div>

</body>
</html>