<?php

include('../app/bootstrap.php');

if (empty($_SESSION['id'])) {
    header('Location: /');
}

if (!empty($_GET['id'])) {
    $user_id = null;
    $id = $_GET['id'];

    if (!empty($_GET['user'])) {
        $user_id = $_GET['user'];
    }

    if (ShoppingList::removeItem($id, $user_id)) {
        header('Location: /');
    }
}