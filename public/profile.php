<?php

include('../app/bootstrap.php');

if (empty($_SESSION['id'])) {
    header('Location: /');
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (User::update([
        'id' => $_POST['user'] ?: $_SESSION['id'],
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name'],
        'email' => $_POST['email'],
        'avatar' => $_FILES['avatar']
    ])) {
        header('Location: /');
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (!empty($_GET['user'])) {
        $user = new User($_GET['user']);
    } else {
        $user = new User();
    }
}

include('../views/profile.php');