<?php

class ShoppingList
{
    public static function getItems($user_id = null)
    {
        global $conn;

        if (!empty($_SESSION['id']) && !$user_id) {
            $user_id = $_SESSION['id'];
        }

        $stmt = $conn->prepare('SELECT * FROM lists WHERE user_id=(:user_id)');
        $stmt->bindParam(':user_id', $user_id);

        $stmt->execute();

        return $stmt->fetchAll();
    }

    public static function getItem($id, $user_id = null)
    {
        global $conn;

        if (!empty($_SESSION['id']) && !$user_id) {
            $user_id = $_SESSION['id'];
        }

        $stmt = $conn->prepare('SELECT * FROM lists WHERE id=(:id) AND user_id=(:user_id)');
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':user_id', $user_id);

        $stmt->execute();

        return $stmt->fetchAll()[0];
    }

    public static function updateItem($title, $description, $user_id = null)
    {
        global $conn;

        if (!empty($_SESSION['id']) && !$user_id) {
            $user_id = $_SESSION['id'];
        }

        $stmt = $conn->prepare('UPDATE lists SET title=(:title), description=(:description), m_time=(:m_time) WHERE user_id=(:user_id)');
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':m_time', date('Y-m-d H:i:s'));
        $stmt->bindParam(':user_id', $user_id);

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

    public static function addItem($title, $description, $user_id)
    {
        global $conn;

        $stmt = $conn->prepare('INSERT INTO lists (title, description, c_time, user_id) VALUES (:title, :description, :c_time, :user_id)');
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':c_time', date('Y-m-d H:i:s'));
        $stmt->bindParam(':user_id', $user_id);

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

    public static function removeItem($id, $user_id = null)
    {
        global $conn;

        if (!empty($_SESSION['id']) && !$user_id) {
            $user_id = $_SESSION['id'];
        }

        $stmt = $conn->prepare('DELETE FROM lists WHERE id=(:id) AND user_id=(:user_id)');
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':user_id', $user_id);

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }
}
