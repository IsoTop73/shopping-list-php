<?php

include('../app/bootstrap.php');

if (empty($_SESSION['id'])) {
    header('Location: /');
}

$user = null;

if (!empty($_POST['user'])) {
    $user = $_POST['user'];
} else {
    $user = $_SESSION['id'];
}

if (!empty($_POST['title'])) {
    if (ShoppingList::addItem($_POST['title'], $_POST['description'], $user)) {
        header('Location: /');
    }
}
