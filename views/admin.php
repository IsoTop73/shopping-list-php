<!DOCTYPE html>
<html lang="en">
<head>
    <?php include '../views/includes/head.php' ?>
</head>
<body>

<?php include '../views/includes/navbar.php'; ?>

<div class="container">
    <ul class="nav nav-tabs">
        <li role="presentation" class="active">
            <a href="#">Users</a>
        </li>
    </ul>
    <table class="table">
        <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Edmin</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user): ?>
            <tr>
                <td><?php echo $user['first_name']; ?></td>
                <td><?php echo $user['last_name']; ?></td>
                <td><?php echo $user['email']; ?></td>
                <td><?php echo $user['admin'] ? 'Yes' : 'No'; ?></td>
                <td>
                    <div class="controls pull-right">
                        <a href="profile.php?user=<?php echo $user['id']; ?>" class="btn btn-primary btn-sm">Edit</a>
                        <a href="posts.php?user=<?php echo $user['id'] ?>" class="btn btn-primary btn-sm">Posts</a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
</body>
</html>