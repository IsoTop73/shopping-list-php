<?php

$_user = new User();

?>

<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Home</a>
        </div>
        <div class="collapse navbar-collapse">

            <?php if (isset($_SESSION['id'])): ?>

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle clearfix" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">
                            <span><?php echo $_user->first_name; ?></span>
                            <div class="avatar">
                                <img src="<?php echo $_user->getAvatar(); ?>">
                            </div>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profile.php">Profile</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="logout.php">Log out</a>
                            </li>
                            <?php if ($_user->admin): ?>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a href="/admin.php">Admin</a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                </ul>

            <?php else: ?>

                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="login.php">Login</a>
                    </li>
                    <li>
                        <a href="register.php">Register</a>
                    </li>
                </ul>

            <?php endif; ?>

        </div>
    </div>
</nav>