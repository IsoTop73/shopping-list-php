<?php

include('../app/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (!empty($_POST['password']) && !empty($_POST['confirm_password'])) {
        if (User::register($_POST['first_name'], $_POST['last_name'], $_POST['email'], $_POST['password'], $_FILES)) {
            header('Location: /');
        }
    } else {
        die('Passwords do not match!');
    }
}

include('../views/register.php');