<?php

include('../app/bootstrap.php');

$user_id = null;

if (empty($_SESSION['id'])) {
    header('Location: /');
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (!empty($_POST['title'])) {
        if (ShoppingList::updateItem($_POST['title'], $_POST['description'])) {
            header('Location: /');
        }
    }
}

if (!empty($_GET['user'])) {
    $user = new User();
    $user_id = $_GET['user'];

    if (!$user->admin) {
        header('Location: /');
    }
}

$item = ShoppingList::getItem($_GET['id'], $user_id);

include('../views/editItem.php');