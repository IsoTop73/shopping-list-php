<?php

$db_config = include('../app/config/database.php');

try {
    $conn = new PDO("mysql:host=$db_config[db_host];dbname=$db_config[db_name];", $db_config['db_user']);
} catch(PDOException $e) {
    die("Connection failed: " . $e->getMessage());
}
