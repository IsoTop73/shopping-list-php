<?php

include('../app/bootstrap.php');

if (empty($_SESSION['id'])) {
    header('Location: /');
}

if (!empty($_GET['user'])) {
    $user = new User($_GET['user']);
} else {
    $user = new User();
}

if (!empty($_GET['user'])) {
    if ($user->id === $_GET['id']) {
        if ($user->deleteAvatar()) {
            header('Location: /');
        }
    }
}
