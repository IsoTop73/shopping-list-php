<!DOCTYPE html>
<html lang="en">
<head>
    <?php include '../views/includes/head.php' ?>
</head>
<body>

    <?php include '../views/includes/navbar.php'; ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">Profile</div>
                    <div class="panel-body">
                    <form enctype="multipart/form-data" action="profile.php" method="POST" class="profile-form">
                            <div class="form-group">
                                <?php if ($user->avatar): ?>
                                    <a href="deleteAvatar.php<?php echo !empty($_GET['user']) ? '?user=' . $user->id : ''; ?>" class="close" aria-label="Close" data-toggle="tooltip"
                                       data-placement="bottom" title="Delete avatar">
                                    <span aria-hidden="true">&times;</span>
                                </a>
                            <?php endif; ?>
                            <label>Avatar</label>
                            <img src="<?php echo $user->getAvatar(); ?>" class="avatar">
                            <input type="file" name="avatar">
                        </div>
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" value="<?php echo $user->first_name; ?>"
                            placeholder="First Name" name="first_name">
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" value="<?php echo $user->last_name; ?>"
                            placeholder="Last Name" name="last_name">
                        </div>
                        <div class="form-group">
                            <label>Email address</label>
                            <input type="email" class="form-control" value="<?php echo $user->email; ?>"
                            placeholder="Email" name="email">
                        </div>
                        <?php if (!empty($_GET['user'])): ?>
                            <input type="hidden" name="user" value="<?php echo $user->id; ?>">
                        <?php endif; ?>
                        <button type="submit" class="btn btn-success btn-block">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>