<?php

class User
{
    private $data = [];
    const avatars_dir = './uploads/users/avatars/';

    public function __construct($user_id = null)
    {
        if (isset($user_id)) {
            $this->data = self::getDataById($user_id);
        } else if (!empty($_SESSION['id'])) {
            $this->data = self::getDataById($_SESSION['id']);
        } 
    }

    public function __get($name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }

        return null;
    }

    static function getUsers()
    {
        global $conn;

        $stmt = $conn->prepare('SELECT * FROM users');

        if ($stmt->execute()) {
            return $stmt->fetchAll();
        }

        return false;
    }

    static function getDataById($id)
    {
        global $conn;

        $stmt = $conn->prepare('SELECT * FROM users WHERE id=(:id)');
        $stmt->bindValue(':id', $id);

        if ($stmt->execute()) {
            return $stmt->fetch();
        }

        return false;
    }

    static function getDataByEmail($email)
    {
        global $conn;

        $stmt = $conn->prepare('SELECT * FROM users WHERE email=(:email)');
        $stmt->bindValue(':email', $email);

        if ($stmt->execute()) {
            return $stmt->fetch();
        }

        return false;
    }

    static function isAdmin() {
        global $conn;

        $stmt = $conn->prepare('SELECT admin FROM users WHERE id=(:id)');
        $stmt->bindValue(':id', $_SESSION['id']);
        $stmt->execute();

        return $stmt->fetch()['admin'];
    }

    static function register($first_name, $last_name, $email, $password, $files)
    {
        global $conn;

        $stmt = $conn->prepare('INSERT INTO users (first_name, last_name, email, password) VALUES (:first_name, :last_name, :email, :password)');
        $stmt->bindValue(':first_name', $first_name);
        $stmt->bindValue(':last_name', $last_name);
        $stmt->bindValue(':email', $email);
        $stmt->bindValue(':password', password_hash($password, PASSWORD_BCRYPT));

        if ($stmt->execute()) {
            self::login($email, $password);

            $id = self::getDataByEmail($email)['id'];

            self::setAvatar($files['avatar'], $id);

            return true;
        }

        return false;
    }

    static function login($email, $password)
    {
        global $conn;

        $stmt = $conn->prepare('SELECT id, password FROM users WHERE email=(:email)');
        $stmt->bindValue(':email', $email);

        if ($stmt->execute()) {
            $user_data = $stmt->fetch();

            if (password_verify($password, $user_data['password'])) {
                $_SESSION['id'] = $user_data['id'];
                return true;
            }
        }

        return false;
    }

    static function update($user)
    {
        global $conn;

        $stmt = $conn->prepare('UPDATE users SET first_name=(:first_name), last_name=(:last_name), email=(:email) WHERE id=(:id)');
        $stmt->bindValue(':first_name', $user['first_name']);
        $stmt->bindValue(':last_name', $user['last_name']);
        $stmt->bindValue(':email', $user['email']);
        $stmt->bindValue(':id', $user['id']);

        self::setAvatar($user['avatar'], $user['id']);

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

    public function getAvatar()
    {
        if ($this->avatar) {
            return $this::avatars_dir . $this->avatar;
        }

        return '/assets/img/avatars/default.png';
    }

    static function setAvatar($avatar, $id = null)
    {
        if (!empty($avatar['name'])) {
            global $conn;

            $ext = pathinfo($avatar['name'], PATHINFO_EXTENSION);

            $avatar_name = uniqid() . '.' . $ext;
            $avatar_path = self::avatars_dir . $avatar_name;

            $stmt = $conn->prepare('UPDATE users SET avatar=(:avatar) WHERE id=(:id)');
            $stmt->bindValue(':avatar', $avatar_name);
            $stmt->bindValue(':id', $id);

            if ($stmt->execute()) {
                move_uploaded_file($avatar['tmp_name'], $avatar_path);
                return true;
            }
        }

        return false;
    }

    public function deleteAvatar()
    {
        global $conn;

        $stmt = $conn->prepare("UPDATE users SET avatar='' WHERE id=(:id)");
        $stmt->bindValue(':id', $this->id);

        if ($stmt->execute()) {

            unlink($this::avatars_dir . $this->avatar);
            return true;
        }

        return false;
    }
}