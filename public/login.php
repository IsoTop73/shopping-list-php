<?php

include('../app/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (!empty($_POST['password']) && !empty($_POST['email'])) {
        if (User::login($_POST['email'], $_POST['password'])) {
            header('Location: /');
        }
    }
}

include('../views/login.php');