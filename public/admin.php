<?php

include('../app/bootstrap.php');

if (empty($_SESSION['id']) || !(User::isAdmin())) {
    header('Location: /');
}

$users = User::getUsers();

include('../views/admin.php');