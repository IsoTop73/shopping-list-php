<!DOCTYPE html>
<html lang="en">
<head>
    <?php include '../views/includes/head.php' ?>
</head>
<body>

    <?php include '../views/includes/navbar.php'; ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">Edit</div>
                    <div class="panel-body">
                        <form id="item-form" action="editItem.php" method="POST">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" placeholder="Title" name="title" value="<?php echo $item['title'] ?>">
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <div class="form-group">
                                    <textarea class="form-control" name="description" placeholder="Description" rows="5"><?php echo $item['description'] ?></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success btn-block">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>